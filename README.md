# React JS Resource.

1. [Kentcdodds](https://kentcdodds.com/)
2. [Epicreact](https://epicreact.dev/)
3. [React-query.tanstack](https://react-query.tanstack.com/).
4. [Redux-toolkit](https://redux-toolkit.js.org/).
5. [Supabase](https://supabase.com/).
6. [Framer](https://www.framer.com/motion/).
7. [Modern-redux-with-redux-toolkit-rtk-and-typescript](https://egghead.io/courses/modern-redux-with-redux-toolkit-rtk-and-typescript-64f243c8).
8. [Build-a-modern-user-interface-with-chakra-ui](https://egghead.io/courses/build-a-modern-user-interface-with-chakra-ui-fac68106)
9. [Resources-by-lee-robinson](https://egghead.io/q/resources-by-lee-robinson)
10. [Introduction-to-state-machines-using-xstate](https://egghead.io/courses/introduction-to-state-machines-using-xstate)
11. [Manage-complex-tic-tac-toe-game-state-in-react](https://egghead.io/courses/manage-complex-tic-tac-toe-game-state-in-react-dddda3f8)
12. [Build-a-saas-product-with-next-js-supabase-and-stripe](https://egghead.io/courses/build-a-saas-product-with-next-js-supabase-and-stripe-61f2bc20)
13. [Testingjavascript](https://testingjavascript.com/)
14. [Mswjs](https://mswjs.io/)
15. [Colinhacks](https://github.com/colinhacks/zod)
16. [Next-auth.js](https://next-auth.js.org/)
17. [Nextjs](https://nextjs.org/).
18. [Prisma](https://www.prisma.io/).
19. [Statelyai/xstate](https://github.com/statelyai/xstate).
20. [Chakra-ui.com](https://chakra-ui.com/).
21. [Cypress](https://www.cypress.io/).
